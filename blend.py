import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import StratifiedKFold

def process(trainFile, testFile, submitFile):

    train = pd.read_csv(trainFile)
    ACTIVITY = 'Activity'

    trainClass = train[ACTIVITY]
    trainData = train.drop(ACTIVITY, axis=1)

    testData = pd.read_csv(testFile)

    X = np.array(trainData)
    Y = np.array(trainClass)
    X_submission = np.array(testData)

    clfs = [RandomForestClassifier(n_estimators=1000, n_jobs=-1, criterion='entropy'),
              RandomForestClassifier(n_estimators=1000, n_jobs=-1, criterion='gini'),
              ExtraTreesClassifier(n_estimators=1000, n_jobs=-1, criterion='entropy'),
              ExtraTreesClassifier(n_estimators=1000, n_jobs=-1, criterion='gini'),
              XGBClassifier(n_estimators=5000, nthread=-1, max_depth=17,learning_rate=0.01,
                            silent=False, subsample=0.8, colsample_bytree=0.8)]

    n_folds = 5
    skf = list(StratifiedKFold(Y, n_folds))

    dataset_blend_train = np.zeros((X.shape[0], len(clfs))) # X.shape[0] = number of rows
    dataset_blend_test = np.zeros((X_submission.shape[0], len(clfs)))

    for j, clf in enumerate(clfs): # [a, b, c] => enumerate => (0, a) (1,b) (2, c)
        print j, clf
        dataset_blend_test_j = np.zeros((X_submission.shape[0], len(skf)))
        for i, (train, test) in enumerate(skf):
            print "Fold", i
            X_train = X[train]
            Y_train = Y[train]
            X_test = X[test]
            Y_test = Y[test]
            clf.fit(X_train, Y_train)
            y_submission = clf.predict_proba(X_test)[:,1]
            dataset_blend_train[test, j] = y_submission
            dataset_blend_test_j[:, i] = clf.predict_proba(X_submission)[:,1]
        dataset_blend_test[:,j] = dataset_blend_test_j.mean(1)

    print
    print "Blending."
    clf = LogisticRegression()
    clf.fit(dataset_blend_train, Y)
    y_submission = clf.predict_proba(dataset_blend_test)[:,1]

    print "Linear stretch of predictions to [0,1]"
    y_min = y_submission.min()
    y_max = y_submission.max()
    Y_test = (y_submission - y_min) / (y_max - y_min)

    size = Y_test.size
    print size
    INDEX = 'MoleculeId'
    index_seq = np.arange(size) + 1
    PROB = 'PredictedProbability'
    data = {PROB : Y_test}
    outframe = pd.DataFrame(data, index=index_seq)
    outframe.to_csv(submitFile, index_label=INDEX)

if __name__=='__main__':
    inputPath = '/Users/reetas/PycharmProjects/CS234/InputData/'
    submitPath = '/Users/reetas/PycharmProjects/CS234/SubmitData/'

    trainFile = inputPath + 'train.csv'
    testFile = inputPath + 'test.csv'
    submitFile = submitPath + 'submitBlend2test.csv'

    process(trainFile, testFile, submitFile)
