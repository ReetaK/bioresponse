import pandas as pd;
from sklearn.ensemble import RandomForestClassifier
import numpy as np

def process(trainFile, testFile, submitFile):
    trainFrame = pd.read_csv(trainFile, header=0);

    #testFrame = pd.read_csv(testFile, header=0);
    ACTIVITY='Activity';

    Y = trainFrame[ACTIVITY]
    X = trainFrame.drop(ACTIVITY, axis=1)
    model = RandomForestClassifier(n_estimators=10000, n_jobs=-1, criterion='entropy')
    model.fit(X,Y);

    X_test = pd.read_csv(testFile, header=0)
    Y_test = model.predict_proba(X_test)[:,1]

    size = Y_test.size
    print size
    INDEX = 'MoleculeId'
    index_seq = np.arange(size) + 1
    PROB = 'PredictedProbability'
    data = {PROB : Y_test}
    outframe = pd.DataFrame(data, index=index_seq)
    outframe.to_csv(submitFile, index_label=INDEX)

if __name__=='__main__':
    INPUT_PATH='/Users/reetas/PycharmProjects/CS234/InputData/';
    SUBMIT_PATH='/Users/reetas/PycharmProjects/CS234/SubmitData/';
    trainFile=INPUT_PATH+'train.csv'
    testFile=INPUT_PATH+'test.csv'
    submitFile=SUBMIT_PATH+'submit1entropy.csv'
    process(trainFile, testFile, submitFile)




