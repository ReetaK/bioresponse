import pandas as pd;
from sklearn.ensemble import ExtraTreesClassifier
import numpy as np

def process(trainFile, testFile, submitFile):
    train = pd.read_csv(trainFile, header=0);
    ACTIVITY = 'Activity';

    Y = train[ACTIVITY];
    X = train.drop(ACTIVITY, axis=1);

    model = ExtraTreesClassifier(n_estimators=10000, n_jobs=-1, criterion='entropy');
    model.fit(X,Y);

    XTest = pd.read_csv(testFile, header=0);
    YTest = model.predict_proba(XTest)[:,1]

    size = YTest.size;
    INDEX = 'MoleculeId'
    indexVal = np.arange(size) + 1;
    PROBABILITY = 'PredictedProbability'
    probval = {PROBABILITY : YTest};
    output = pd.DataFrame(probval, index = indexVal)
    output.to_csv(submitFile, index_label=INDEX)

if __name__ == '__main__':
    InputPath = '/Users/reetas/PycharmProjects/CS234/InputData/';
    SubmitPath = '/Users/reetas/PycharmProjects/CS234/SubmitData/';
    trainFile = InputPath + 'train.csv'
    testFile = InputPath + 'test.csv'
    submitFile = SubmitPath + 'submit2entropy.csv'
    process(trainFile, testFile, submitFile)