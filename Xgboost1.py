import xgboost as xgb;
import pandas as pd;
import numpy as np;

def process(trainFile, testFile, submitFile):
    train = pd.read_csv(trainFile);

    ACTIVITY='Activity'
    Y = train[ACTIVITY].values
    X = np.array(train.drop(ACTIVITY, axis=1))

    clf = xgb.XGBClassifier(n_estimators=5000, nthread=-1, max_depth=17,
                        learning_rate=0.01, silent=False, subsample=0.8, colsample_bytree=0.8)

    clf.fit(X,Y)

    test = pd.read_csv(testFile)
    XT = np.array(test)

    Y_test = clf.predict_proba(XT)[:,1]
    size = Y_test.size
    print size
    INDEX = 'MoleculeId'
    index_seq = np.arange(size) + 1
    PROB = 'PredictedProbability'
    data = {PROB : Y_test}
    outframe = pd.DataFrame(data, index=index_seq)
    outframe.to_csv(submitFile, index_label=INDEX)

if __name__=='__main__':
    INPUT_PATH='/Users/reetas/PycharmProjects/CS234/InputData/';
    SUBMIT_PATH='/Users/reetas/PycharmProjects/CS234/SubmitData/';
    trainFile=INPUT_PATH+'train.csv'
    testFile=INPUT_PATH+'test.csv'
    submitFile=SUBMIT_PATH+'xgboost1.csv'
    process(trainFile, testFile, submitFile)




