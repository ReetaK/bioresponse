import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier
import numpy as np

def process(trainFile, testFile, submitFile):
    train = pd.read_csv(trainFile, header = 0)
    ACTIVITY = 'Activity'

    Y = train[ACTIVITY]
    X = train.drop(ACTIVITY, axis = 1)

    model = GradientBoostingClassifier(learning_rate=0.05, subsample=0.5, max_depth=6, n_estimators=1000)
    model.fit(X,Y)

    XTest = pd.read_csv(testFile, header = 0)
    YTest = model.predict_proba(XTest)[:,1]

    size = YTest.size
    INDEX = 'MoleculeId'
    indexval = np.arange(size) + 1
    PROBABILITY = 'PredictedProbability'
    probval = {PROBABILITY : YTest}
    output = pd.DataFrame(probval, index = indexval)
    output.to_csv(submitFile, index_label = INDEX)


if __name__ == '__main__':
    inputPath = '/Users/reetas/PycharmProjects/CS234/InputData/'
    submitPath = '/Users/reetas/PycharmProjects/CS234/SubmitData/'
    trainFile = inputPath + 'train.csv'
    testFile = inputPath + 'test.csv'
    submitFile = submitPath + 'submit3_1000ne.csv'
    process(trainFile, testFile, submitFile)